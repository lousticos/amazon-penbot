// Ces deux librairies vont nous permettre de contrôller les moteurs pas à pas ainsi que le servo-moteur
#include <Servo.h>
#include <Stepper.h>

//On a besoin de la librairie mathématique pour pouvoir utiliser la fonction arc-tangente permettant au robot de revenir à son point d'origine si il dépasse la zone de dessin
#include <math.h>

// Définition des pins des moteurs

#define SERVO_PIN 3
#define RIGHT_MOTOR_A 11
#define RIGHT_MOTOR_B 9
#define RIGHT_MOTOR_C 10
#define RIGHT_MOTOR_D 8

#define LEFT_MOTOR_A 4
#define LEFT_MOTOR_B 6
#define LEFT_MOTOR_C 5
#define LEFT_MOTOR_D 7

// Définition des paramètres physiques du robot
double wheelPerimeter = 39;
double wheelDistance = 11.59; //cm
double rotationPerimeter = wheelDistance*M_PI; //cm

// Définition des paramètres des moteurs pas à pas
int stepsPerRevolution = 2048;
double stepsPerCm = stepsPerRevolution/wheelPerimeter;
int revolutionPerMinute = 15;

// Définition du servo-moteur
Servo servo;
int servoLowAngle = 0;
int servoHighAngle = 25;

// Définition des moteurs pas à pas
Stepper rightStepper(stepsPerRevolution, RIGHT_MOTOR_A, RIGHT_MOTOR_B, RIGHT_MOTOR_C, RIGHT_MOTOR_D);
Stepper leftStepper(stepsPerRevolution, LEFT_MOTOR_A, LEFT_MOTOR_B, LEFT_MOTOR_C, LEFT_MOTOR_D);

// Définition des paramètres de dessin du robot
double lineSize = 3; // taille de la ligne droite tracée à chaque étape
double maxTurnAngle = 90; // angle maximal à chaque fois que le robot tourne
double penUpProbability = 2; // Probabilité (en pourcent) que le stylo se lève à une étape du dessin

// Variables permettant d'enregistrer la position du robot au fur et à mesure de son avancée.

double robotX = 0.0;
double robotY = 0.0;
double robotAngle = 0.0;

// Définition des paramètres de la zone de dessin.

double canvasX = 15;
double canvasY = 15;

// Cette variable permet d'arrêter le robot lorsqu'il va dépasser la zone de dessin autorisée.
bool keepGoing = true;


// prototype des fonctions
void drawSquare(double side);
void drawTriangle(double side);
void drawTriforce(double smallSide);
void goForward(double distance);
void turn(double angle);
void penUp(bool penState);
double degToRad(double angle);
double radToDeg(double angle);

void setup()
{

  Serial.begin(115200);
  
  // On initialise le servo-moteur
  servo.attach(SERVO_PIN);
  
  // On initialise la vitesse des moteurs pas à pas
  rightStepper.setSpeed(revolutionPerMinute);
  leftStepper.setSpeed(revolutionPerMinute);

  // On définit les pins controllant les moteurs comme des sorties
  pinMode(RIGHT_MOTOR_A, OUTPUT);
  pinMode(RIGHT_MOTOR_B, OUTPUT);
  pinMode(RIGHT_MOTOR_C, OUTPUT);
  pinMode(RIGHT_MOTOR_D, OUTPUT);
  
  pinMode(LEFT_MOTOR_A, OUTPUT);
  pinMode(LEFT_MOTOR_B, OUTPUT);
  pinMode(LEFT_MOTOR_C, OUTPUT);
  pinMode(LEFT_MOTOR_D, OUTPUT);

  pinMode(SERVO_PIN, OUTPUT);

  // On initialise le générateur de nombre aléatoire
  randomSeed(analogRead(0));

  //Phase laissant le temps à l'utilisateur de téléverser un nouveau code avant que le robot ne commence à se déplacer
  penUp(true);
  delay(1000);
  penUp(false);
  delay(1000);
  penUp(true);
  delay(1000);
  penUp(false);
  delay(1000);
  penUp(true);
  delay(1000);
  
  keepGoing = true;
}

void loop()
{
  // Dessin aléatoire
  goForward(lineSize);
  double turnAngle = random(-maxTurnAngle, maxTurnAngle+1);
  turn(turnAngle);
  
  if(random(0,100) < penUpProbability)
  {
    penUp(true); 
  }
  else
  {
    penUp(false);
  }

  // Actualisation de la position du robot
  robotAngle += turnAngle;
  robotAngle = int(robotAngle) % 360;
  robotX += lineSize*cos(degToRad(robotAngle));
  robotY += lineSize*sin(degToRad(robotAngle));

  Serial.print(robotX);
  Serial.print(";");
  Serial.println(robotY);
  
  // Si le robot a atteint l'extrémité de la zone de dessin on fait demi-tour
  if(abs(robotX) >= canvasX/2 || abs(robotY) >= canvasY/2)
  {
    turn(180);
    robotAngle += 180;
    robotAngle = int(robotAngle) % 360;
    goForward(lineSize);
    robotX += lineSize*cos(degToRad(robotAngle));
    robotY += lineSize*sin(degToRad(robotAngle));
  }
}

// Fonctions dessinant des motifs de base afin de tester la calibration du robot

void drawSquare(double side)
{
  delay(100);
  penUp(false);
  goForward(side);
  turn(90);
  goForward(side);
  turn(90);
  goForward(side);
  turn(90);
  goForward(side);
  turn(90);
  penUp(true);
  delay(100);
}

void drawTriangle(double side)
{
  penUp(false);
  goForward(side);
  turn(180-60);
  goForward(side);
  turn(180-60);
  goForward(side);
  turn(180-60);
  penUp(true); 
}

void drawTriforce(double smallSide)
{
  drawTriangle(smallSide);
  goForward(smallSide);
  drawTriangle(smallSide);
  turn(60);
  goForward(smallSide);
  turn(60);
  drawTriangle(smallSide);
  turn(60);
  goForward(smallSide);
  turn(60);
  goForward(smallSide);
  turn(180-60);
}

// Fonctions contrôllant les moteurs du robot afin d'avancer, de tourner et de lever/baisser le stylo

void goForward(double distance)
{
  int i=0;
  int numberOfSteps = abs(distance * stepsPerCm);
  for(i=0; i<numberOfSteps; i++)
  {
    if(distance<0)
    {
      leftStepper.step(1);
      rightStepper.step(-1); 
    }
    else  
    {
      leftStepper.step(-1);
      rightStepper.step(1);
    }
  }
}

void turn(double angle)
{
  //penUp(true);
  int i = 0;
  int numberOfSteps = (abs(angle) / 360) * (rotationPerimeter * stepsPerCm);

  for(i=0; i<numberOfSteps; i++)
  {
    if(angle<0)
    {
      leftStepper.step(1);
      rightStepper.step(1); 
    }
    else  
    {
      leftStepper.step(-1);
      rightStepper.step(-1);
    }
  }
  //penUp(false);
}

void penUp(bool penState)
{
  if(penState)
    servo.write(servoHighAngle);
  else
    servo.write(servoLowAngle);
}

// Fonctions de conversions degrés/radians

double degToRad(double angle)
{
  return(angle/180*M_PI);
}

double radToDeg(double angle)
{
  return(angle/M_PI*180);
}
